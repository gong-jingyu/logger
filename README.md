# logger

## 使用POM

```tex
1、下载项目：git clone git@gitee.com:gong-jingyu/logger.git
2、进入到项目的根目录：cd logger/
3、mvn install
4、在项目中引入坐标
	<dependency>
        <groupId>com.gjy</groupId>
        <artifactId>logger</artifactId>
        <version>1.0</version>
    </dependency>
5、然后就可以在项目中使用了
```

## 基本使用

注意不要引错包

```java
package com.gjy;

import com.gjy.factory.LoggerFactory;

public class Test {
    private static final Logger LOGGER = LoggerFactory.getFactory(Test.class);

    public static void main(String[] args) {
        LOGGER.info("Hello");
    }
}

结果：
[2021-11-28 16:29:32:629]  [main]  [INFO]  [com.gjy.Test]  [main]  -  Hello
```

## 输出方向

```tex
控制台          console
文件            file
按天拆分文件     dailyFile
按大小拆分文件   rollingFile
数据库	         database
ElasticSearch   es
```

## 日志级别

```tex
INFO、DEBUG、WRAN、ERROR
```

## 日志格式

```tex
standard: [yyyy-MM-dd HH:mm:ss:SSS] [Thread] [Level] [ClassName] [MethodName] - [Message]
```

## 配置文件模板

### 注意

```tex
所有配置的名字请勿修改
例如：logger.properties中的key，数据库表格中的表名和字段名，ES的索引名和字段名

因时间有限，暂时不支持dailyFile、rollingFile，如果有需要，可以自己补充

使用时必须在 resources 目录下创建 logger.properties 文件，否则会报错，文件找不到
```

### logger.properties

```properties
database.driver=com.mysql.cj.jdbc.Driver
database.url=jdbc:mysql://localhost:3306/logger
database.username=root
database.password=Oracle12

es.url=localhost
es.port=9200
es.protocol=http

file.name=logger.log
file.path=D:/last/code/WinterTrainingProgram/logging/logger/src/main/resources/
file.daily=false

user=file
```

### 数据库表格

```sql
CREATE TABLE IF NOT EXISTS logger(
    `id` BIGINT PRIMARY KEY ,
    `date_time` DATETIME NOT NULL ,
    `thread` VARCHAR(255) NOT NULL ,
    `level` ENUM('INFO','DEBUG','WARN','ERROR') NOT NULL ,
    `class_name` VARCHAR(255) NOT NULL ,
    `method_name` VARCHAR(255) NOT NULL ,
    `message` VARCHAR(255) NOT NULL,
    `throwable` VARCHAR(255)
);
```

### ES结构

```json
PUT /logger
{
  "mappings": {
    "properties": {
      "datetime": {
        "type": "date"
      },
      "thread": {
        "type": "text",
        "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
      },
      "level": {
        "type": "keyword"
      },
      "classname": {
        "type": "text",
        "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
      },
      "methodname": {
        "type": "text",
        "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
      },
      "message": {
        "type": "text",
        "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
      },
      "throwable": {
        "type": "text",
        "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
      }
    }
  }
}
```

### logger.properties讲解

```tex
database.driver                              有默认值，为mysql
database.url                                 无默认值，如果user为database 则必须配置
database.username=root                       无默认值，如果user为database 则必须配置
database.password=Oracle12                   无默认值，如果user为database 则必须配置

es.url                                       无默认值，如果user为es 则必须配置
es.port                                      有默认值，为 9200
es.protocol                                  有默认值，为 http

file.name                                    有默认值，为 logger.log
file.path                                    有默认值，为 System.getProperty("user.dir")，自己配置，必须                                              在最后加上 /
file.daily                                   有默认值，为 false，暂时不需要配，只影响文件名

user                                         选择输出的方式：console 控制台输出，file 输出到文件中，database 输出到数据库中，es 输出到ElasticSearch中
```

