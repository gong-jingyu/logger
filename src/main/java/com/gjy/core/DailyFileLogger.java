package com.gjy.core;

import com.gjy.Logger;

/**
 * @author 宫静雨
 * @version 1.0
 */
public class DailyFileLogger implements Logger {
    private Class<?> clazz;

    public DailyFileLogger(Class<?> clazz) {
        this.clazz = clazz;
    }

    @Override
    public void info(String message) {

    }

    @Override
    public void info(String format, Object... arg) {

    }

    @Override
    public void info(String message, Throwable t) {

    }

    @Override
    public void debug(String message) {

    }

    @Override
    public void debug(String format, Object... arg) {

    }

    @Override
    public void debug(String message, Throwable t) {

    }

    @Override
    public void warn(String message) {

    }

    @Override
    public void warn(String format, Object... arg) {

    }

    @Override
    public void warn(String message, Throwable t) {

    }

    @Override
    public void error(String message) {

    }

    @Override
    public void error(String format, Object... arg) {

    }

    @Override
    public void error(String message, Throwable t) {

    }
}
