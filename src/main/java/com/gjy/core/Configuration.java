package com.gjy.core;

import com.gjy.util.FieldTrans;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 用于解析 logger.properties 配置文件
 * 默认输出到控制台，不需要配置
 * @author 宫静雨
 * @version 1.0
 */
public class Configuration {

   /* 配置你使用哪一个: console,file,database,es。默认是console */
   private String use;

   private static final String FILE_NAME = "logger.properties";

   public static Map<String,String> init() {
      Map<String,String> map = new HashMap<>();
      InputStream is = Configuration.class.getClassLoader().getResourceAsStream(FILE_NAME);
      Properties properties = new Properties();
      try {
         properties.load(is);
         properties.forEach((k,v) -> map.put(FieldTrans.spot2cap(k.toString()),v.toString()));
      } catch (IOException e) {
         e.printStackTrace();
      }
      return map;
   }

}
