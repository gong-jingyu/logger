package com.gjy.core;

import com.gjy.Level;
import com.gjy.Logger;
import com.gjy.util.StringUtil;

/**
 * @author 宫静雨
 * @version 1.0
 */
public class ConsoleLogger implements Logger {

    private Class<?> clazz;

    public ConsoleLogger(Class<?> clazz) {
        this.clazz = clazz;
    }

    @Override
    public void info(String message) {
        System.out.println(StringUtil.standard(clazz, message, Level.INFO));
    }

    @Override
    public void info(String format, Object... arg) {
        StringBuilder sb = new StringBuilder(format);
        for (Object o : arg) {
            sb.replace(sb.indexOf("{"), sb.indexOf("}") + 1, o.toString());
        }
        System.out.println(StringUtil.standard(clazz, sb.toString(), Level.INFO));
    }

    @Override
    public void info(String message, Throwable t) {
        String standard = StringUtil.standard(clazz, message, Level.INFO);
        System.out.println(standard + "  " + t);
    }

    @Override
    public void debug(String message) {
        System.out.println(StringUtil.standard(clazz, message, Level.DEBUG));
    }

    @Override
    public void debug(String format, Object... arg) {
        StringBuilder sb = new StringBuilder(format);
        for (Object o : arg) {
            sb.replace(sb.indexOf("{"), sb.indexOf("}") + 1, o.toString());
        }
        System.out.println(StringUtil.standard(clazz, sb.toString(), Level.DEBUG));
    }

    @Override
    public void debug(String message, Throwable t) {
        String standard = StringUtil.standard(clazz, message, Level.DEBUG);
        System.out.println(standard + "  " + t);
    }

    @Override
    public void warn(String message) {
        System.out.println(StringUtil.standard(clazz, message, Level.WARN));
    }

    @Override
    public void warn(String format, Object... arg) {
        StringBuilder sb = new StringBuilder(format);
        for (Object o : arg) {
            sb.replace(sb.indexOf("{"), sb.indexOf("}") + 1, o.toString());
        }
        System.out.println(StringUtil.standard(clazz, sb.toString(), Level.WARN));
    }

    @Override
    public void warn(String message, Throwable t) {
        String standard = StringUtil.standard(clazz, message, Level.WARN);
        System.out.println(standard + "  " + t);
    }

    @Override
    public void error(String message) {
        System.out.println(StringUtil.standard(clazz, message, Level.ERROR));
    }

    @Override
    public void error(String format, Object... arg) {
        StringBuilder sb = new StringBuilder(format);
        for (Object o : arg) {
            sb.replace(sb.indexOf("{"), sb.indexOf("}") + 1, o.toString());
        }
        System.out.println(StringUtil.standard(clazz, sb.toString(), Level.ERROR));
    }

    @Override
    public void error(String message, Throwable t) {
        String standard = StringUtil.standard(clazz, message, Level.ERROR);
        System.out.println(standard + "  " + t);
    }
}
