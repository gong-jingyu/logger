package com.gjy;

/**
 * @author 宫静雨
 * @version 1.0
 */
public interface Logger {

    void info(String message);

    void info(String format,Object...arg);

    void info(String message,Throwable t);

    void debug(String message);

    void debug(String format,Object...arg);

    void debug(String message,Throwable t);

    void warn(String message);

    void warn(String format,Object...arg);

    void warn(String message,Throwable t);

    void error(String message);

    void error(String format,Object...arg);

    void error(String message,Throwable t);
}
