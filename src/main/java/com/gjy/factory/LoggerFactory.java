package com.gjy.factory;

import com.gjy.Logger;
import com.gjy.core.*;

/**
 * @author 宫静雨
 * @version 1.0
 */
public class LoggerFactory {

    private static String use;

    static {
        use = Configuration.init().getOrDefault("user","console");
    }

    public static Logger getFactory(Class<?> clazz) {
        Logger logger;
        if ("console".equals(use)) {
            logger = new ConsoleLogger(clazz);
        }else if ("file".equals(use)) {
            logger = new FileLogger(clazz);
        }else if ("database".equals(use)) {
            logger = new DataBaseLogger(clazz);
        }else if ("es".equals(use)) {
            logger = new ESLogger(clazz);
        }else if ("daily".equals(use)) {
            logger = new DailyFileLogger(clazz);
        } else {
            throw new IllegalArgumentException("配置文件 logger.properties 中 use 参数配置错误\n" +
                    "可以选择的有：console,file,database,es");
        }
        return logger;
    }
}
