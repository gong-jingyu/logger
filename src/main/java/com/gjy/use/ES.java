package com.gjy.use;

import com.gjy.core.Configuration;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.util.Map;

/**
 * @author 宫静雨
 * @version 1.0
 */
public class ES {
    /* ElasticSearch配置项 */
    private String esUrl;
    private Integer esPort;
    private String esProtocol;

    public ES() {
        Map<String, String> map = Configuration.init();
        esUrl = map.get("esUrl");
        esPort = Integer.valueOf(map.getOrDefault("esPort","9200"));
        esProtocol = map.getOrDefault("esProtocol","http");
    }

    public RestHighLevelClient client() {
        return new RestHighLevelClient(RestClient.builder(new HttpHost(esUrl, esPort, esProtocol)));
    }
}
