package com.gjy.use;

import com.gjy.Logger;
import com.gjy.core.Configuration;
import com.gjy.factory.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author 宫静雨
 * @version 1.0
 */
public class DataBase {
    private String databaseUrl;
    private String databaseUsername;
    private String databasePassword;

    public DataBase() {
        Map<String, String> map = Configuration.init();
        /* 数据库配置项 */
        String databaseDriver = map.getOrDefault("databaseDriver", "com.mysql.cj.jdbc.Driver");
        databaseUrl = map.get("databaseUrl");
        databaseUsername = map.get("databaseUsername");
        databasePassword = map.get("databasePassword");
        try {
            Class.forName(databaseDriver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
