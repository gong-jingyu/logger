package com.gjy.use;

import com.gjy.core.Configuration;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * @author 宫静雨
 * @version 1.0
 */
public class File {
    /* 文件配置项 */
    private String fileName;
    private String filePath;
    private Boolean fileDaily;

    public File() {
        Map<String, String> map = Configuration.init();
        fileName = map.getOrDefault("fileName","logger.log");
        filePath = map.getOrDefault("filePath",System.getProperty("user.dir"));
        fileDaily = Boolean.valueOf(map.getOrDefault("fileDaily","false"));
    }

    public FileWriter get() throws IOException {
        String format = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now());

        if (!fileDaily) {
            Path path = Paths.get(filePath + java.io.File.separator + fileName);
            if (!Files.exists(path)) {
                Files.createFile(path);
            }
            return new FileWriter(path.toFile(),true);
        }else {
            Path path = Paths.get(filePath + java.io.File.separator + format + "_" +fileName);
            if (!Files.exists(path)) {
                Files.createDirectories(Paths.get(filePath));
                Files.createFile(path);
            }
            return new FileWriter(path.toFile(),true);
        }
    }
}
