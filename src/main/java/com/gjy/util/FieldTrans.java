package com.gjy.util;

/**
 * @author 宫静雨
 * @version 1.0
 */
public final class FieldTrans {

    /* database.url -> databaseUrl */
    public static String spot2cap(String str) {
        StringBuilder sb = new StringBuilder();
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '.') {
                i++;
                chars[i] = Character.toUpperCase(chars[i]);
            }
            sb.append(chars[i]);
        }
        return sb.toString();
    }

    /* databaseUrl -> database.url */
    public static String cap2spot(String str) {
        StringBuilder sb = new StringBuilder();
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] >= 'A' && chars[i] <= 'Z') {
                sb.append(".").append(Character.toLowerCase(chars[i]));
                i++;
            }
            sb.append(chars[i]);
        }
        return sb.toString();
    }

    /* className -> class_name */
    public static String pojo2table(String str) {
        StringBuilder sb = new StringBuilder();
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] >= 'A' && chars[i] <= 'Z') {
                sb.append("_").append(Character.toLowerCase(chars[i]));
                i++;
            }
            sb.append(chars[i]);
        }
        return sb.toString();
    }

    /* class_name -> className */
    public static String table2pojo(String str) {
        StringBuilder sb = new StringBuilder();
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '_') {
                i++;
                chars[i] = Character.toUpperCase(chars[i]);
            }
            sb.append(chars[i]);
        }
        return sb.toString();
    }
}
