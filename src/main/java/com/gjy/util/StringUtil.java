package com.gjy.util;

import com.gjy.Level;

import java.util.Date;

/**
 * @author 宫静雨
 * @version 1.0
 */
public class StringUtil {

    private static Date now = new Date(System.currentTimeMillis());

    public static String standard(Class<?> clazz, String message, Level level) {
        return "[" + DateUtil.format(now) + "]" + "  " +
                "[" + Thread.currentThread().getName() + "]" + "  " +
                "[" + level + "]" + "  " +
                "[" + clazz.getName() + "]" + "  " +
                "[" + Thread.currentThread().getStackTrace()[3].getMethodName() + "]" + "  " +
                "-" + "  " + message;
    }

    public static String standardFile(Class<?> clazz, String message, Level level) {
        return "[" + DateUtil.format(now) + "]" + "  " +
                "[" + Thread.currentThread().getName() + "]" + "  " +
                "[" + level + "]" + "  " +
                "[" + clazz.getName() + "]" + "  " +
                "[" + Thread.currentThread().getStackTrace()[4].getMethodName() + "]" + "  " +
                "-" + "  " + message;
    }
}
