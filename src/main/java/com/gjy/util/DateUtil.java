package com.gjy.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 宫静雨
 * @version 1.0
 */
public class DateUtil {

    private static final String STANDARD_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS";

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat(STANDARD_FORMAT);

    public static String format(Date date) {
        return FORMAT.format(date);
    }

    public static Date parse(String text) {
        try {
            return FORMAT.parse(text);
        } catch (ParseException e) {
            throw new RuntimeException("时间格式错误,标准时间格式：" + STANDARD_FORMAT);
        }
    }

    public static Long nanoTime() {
        return System.nanoTime();
    }

    public static Long currentTimeMillis() {
        return System.currentTimeMillis();
    }
}
