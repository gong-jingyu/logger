package com.gjy;

/**
 * 日志级别
 * @author 宫静雨
 * @version 1.0
 */
public enum Level {

    INFO,DEBUG,WARN,ERROR
}
